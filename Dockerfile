# https://medium.freecodecamp.org/how-to-deploy-a-node-js-application-to-amazon-web-services-using-docker-81c2a2d7225b
# https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

# we are building our Docker image using the official Node.js image from Dockerhub (a repository for base images).
FROM node:9.11.1-alpine

# RUN: allow us to execute a command for anything you want to do
RUN mkdir -p /usr/src/grab-scraper

# WORKDIR: establishes the subdirectory we created as the working directory
# for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile
WORKDIR /usr/src/grab-scraper

# COPY let us copy files from a source to a destination
COPY . .

RUN npm install strapi@alpha -g

RUN cd /usr/src/grab-scraper

RUN npm install

# EXPOSE instruction informs Docker that the container listens on the specified network ports at runtime.
EXPOSE 1337

CMD ["strapi", "start" ]
