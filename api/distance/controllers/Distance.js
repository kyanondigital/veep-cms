'use strict';

/**
 * Distance.js controller
 *
 * @description: A set of functions called "actions" for managing `Distance`.
 */

module.exports = {

  /**
   * Retrieve distance records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.distance.search(ctx.query);
    } else {
      return strapi.services.distance.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a distance record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.distance.fetch(ctx.params);
  },

  /**
   * Count distance records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.distance.count(ctx.query);
  },

  /**
   * Create a/an distance record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.distance.add(ctx.request.body);
  },

  /**
   * Update a/an distance record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.distance.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an distance record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.distance.remove(ctx.params);
  }
};
