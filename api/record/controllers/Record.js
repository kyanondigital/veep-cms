'use strict';

/**
 * Record.js controller
 *
 * @description: A set of functions called "actions" for managing `Record`.
 */

module.exports = {

  /**
   * Retrieve record records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.record.search(ctx.query);
    } else {
      return strapi.services.record.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a record record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.record.fetch(ctx.params);
  },

  /**
   * Count record records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.record.count(ctx.query);
  },

  /**
   * Create a/an record record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.record.add(ctx.request.body);
  },

  /**
   * Update a/an record record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.record.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an record record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.record.remove(ctx.params);
  }
};
